from domain.interfaces.database import IDatabase
from infra.database import MongoDB


class DatabaseFactory:
    @classmethod
    def get_implementation(cls, lazy=True) -> IDatabase:
        return MongoDB(lazy=lazy)
