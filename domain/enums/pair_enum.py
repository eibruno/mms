from enum import Enum, IntEnum


class PairEnum(str, Enum):
    BRLBTC = "BRLBTC"
    BRLETH = "BRLETH"

    @classmethod
    def cursor(cls):
        for element in {cls.BRLBTC, cls.BRLETH}:
            yield element.value


class RangeEnum(IntEnum, Enum):
    TWENTY = 20
    FIFTY = 50
    TWO_HUNDRED = 200


if __name__ == "__main__":
    for el in PairEnum.cursor():
        print(el)
