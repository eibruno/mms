from pydantic import BaseModel


class CandleModel(BaseModel):
    timestamp: int
    open: float
    close: float
    high: float
    low: float
    volume: float
