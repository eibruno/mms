from pydantic import BaseModel

from domain.enums.pair_enum import PairEnum


class MmsModel(BaseModel):
    pair: PairEnum
    timestamp: int
    mms_20: float
    mms_50: float
    mms_200: float

    def get_identifier(self) -> dict:
        return {
            "pair": self.pair,
            "timestamp": self.timestamp
        }
