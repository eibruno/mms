from pydantic import BaseModel, ValidationError, validator
from typing import Union
from domain.enums.pair_enum import PairEnum, RangeEnum
from datetime import datetime, timedelta
from config.config import REQUEST_MAX_RANGE


class PairResponseModel(BaseModel):
    timestamp: int
    mms: float


class PairRequestModel(BaseModel):
    pair: PairEnum
    from_time: int
    to_time: int
    range: RangeEnum

    @validator('from_time', check_fields=False)
    def from_must_be_timestamp(cls, v):
        try:
            datetime.fromtimestamp(v)
            return v
        except Exception as err:
            raise ValueError("from must be a timestamp")

    @validator('from_time', check_fields=False)
    def from_must_be_after(cls, v):
        from_date = datetime.fromtimestamp(v)
        today = datetime.now()
        print((today - from_date).days, REQUEST_MAX_RANGE)

        if (today - from_date).days > REQUEST_MAX_RANGE:
            raise ValueError(f"from argument must be after {(today - timedelta(days=REQUEST_MAX_RANGE)).timestamp()} days")
        return v

    @validator('to_time', check_fields=False)
    def to_must_be_timestamp(cls, v):
        try:
            datetime.fromtimestamp(v)
            return v
        except Exception as err:
            raise ValueError("to argument must be a valid timestamp")
