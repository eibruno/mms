from abc import ABCMeta, abstractmethod

from pymongo import MongoClient


class IDatabase(metaclass=ABCMeta):
    @abstractmethod
    def health_check(self):
        raise NotImplemented("health_check needs to be implemented")

    @abstractmethod
    def connection(self) -> MongoClient:
        raise NotImplemented("connection needs to be implemented")
