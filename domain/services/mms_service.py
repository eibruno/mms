import logging
from datetime import datetime
from typing import List, Union

from domain.enums.pair_enum import RangeEnum, PairEnum
from domain.interfaces.database import IDatabase
from domain.models.candle_model import CandleModel
from domain.models.mms_model import MmsModel
from infra.mms_repository import MmsRepository


class MmsService:
    def __init__(self, db: IDatabase):
        self._repository = MmsRepository(db)

    def health_check(self):
        return self._repository.health_check()

    def save_mms(self, candles: List[CandleModel], pair: PairEnum):
        mms_50 = 0
        mms_20 = 0
        mms_200 = 0

        last_candle: Union[MmsModel, None] = None

        for index, candle in enumerate(candles):
            if index >= RangeEnum.TWENTY:
                mms_20 += self._get_mms(candles, index, RangeEnum.TWENTY)
            if index >= RangeEnum.FIFTY:
                mms_50 += self._get_mms(candles, index, RangeEnum.FIFTY)
            if index >= RangeEnum.TWO_HUNDRED:
                mms_200 += self._get_mms(candles, index, RangeEnum.TWO_HUNDRED)

            model = MmsModel(
                pair=pair,
                timestamp=candle.timestamp,
                mms_50=mms_50,
                mms_20=mms_20,
                mms_200=mms_200
            )

            self._repository.save(model)

            if last_candle and (
                    datetime.fromtimestamp(model.timestamp) - datetime.fromtimestamp(last_candle.timestamp)
            ).days > 1:
                logging.critical(
                    f"Missing day found between: {last_candle.timestamp} and {model.timestamp} for Pair: {pair}"
                )

            last_candle = model

    def get_range(self, pair: PairEnum, from_time: int, to_time: int) -> List[MmsModel]:
        return list(self._repository.find_range(pair, from_time, to_time, range_precision=True))

    def get_count(self) -> int:
        return self._repository.get_count()

    @staticmethod
    def _get_mms(candles: List[CandleModel], from_index: int, days: RangeEnum):
        return sum(c.close for c in candles[from_index - days: from_index - 1]) / days

