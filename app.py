import logging

import uvicorn
from fastapi import FastAPI

from cmd import parse_args
from config.config import API_PORT
from crosscutting.logger import get_logger
from presentation.routers.pair_routers import router as pair_router

app = FastAPI(title="PairAPI")
get_logger()

app.include_router(
    router=pair_router,
    tags=["pair"]
)

if __name__ == "__main__":
    if not parse_args():
        uvicorn.run(app, host="0.0.0.0", port=API_PORT)
