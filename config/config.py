import os

from dotenv import load_dotenv

load_dotenv()


API_PORT: int = int(os.getenv("PORT", 80))
REQUEST_MAX_RANGE: int = int(os.getenv("REQUEST_MAX_RANGE", 365))
DATABASE_HOST = os.getenv("DB_HOST", "0.0.0.0")
DATABASE_PORT = int(os.getenv("DB_PORT", 27017))
