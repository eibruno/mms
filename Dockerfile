FROM python:3.9-slim

ARG INDEX_URL

ADD . /app
WORKDIR /app

RUN apt-get update -y
RUN python -m ensurepip --default-pip
RUN pip install  --upgrade pip setuptools
RUN pip install  -r requirements.txt

EXPOSE 80

ENTRYPOINT ["./pre-up.sh"]
CMD ["python", "app.py"]
