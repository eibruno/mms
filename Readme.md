##Estratégia para incremento diário da tabela.

Criar um cronjob no Kubernetes utilizando a imagem ./k8s/cron.yml, que irá iniciar automaticamente todos os dias às 00h. 
Neste Job, o projeto identificará se a base de dados está vazia, e caso esteja, fará uma carga do CandlesAPI dos últimos 365 dias.
Caso a base não esteja vazia, uma carga das últimas 24h será realizada.

Caso a CandlesAPI esteja indisponível, foi incluído em código um circuit-breaker para nova tentativa, porém após 5 tentativas, o job deverá morrer e o cronjob reiniciará o job até 3x.
Neste caso, eu implementaria em um Istio ou algo semelhante um circuit-break.

Como a resposta do CandlesAPI é ordenada, caso um dia falte no retorno, um log crítico é criado, então eu implementaria um monitor nestes logs.

##Doc API
Swagger: http://localhost:8000/docs

##Para Rodar
```docker-compose up --build```