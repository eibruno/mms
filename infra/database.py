import os

from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from config.config import DATABASE_HOST, DATABASE_PORT

from domain.interfaces.database import IDatabase


class MongoDB(IDatabase):
    _CONN: MongoClient = None
    _HOST = DATABASE_HOST
    _PORT = DATABASE_PORT

    def __init__(self, lazy=True, host: str = None, port: int = None):
        assert host is None or isinstance(host, str), "host should be str"
        assert port is None or isinstance(port, int), "port should be int"
        assert isinstance(lazy, bool), "lazy should be boolean"

        if host:
            self._HOST = host

        if port:
            self._PORT = port

        if not lazy:
            self.connect()

    @property
    def connection(self):
        if self._CONN is None:
            self.connect()
        return self._CONN

    def connect(self):
        self._CONN = MongoClient(host=self._HOST, port=self._PORT)

    def health_check(self):
        try:
            if self._CONN is None:
                MongoClient(host=self._HOST, port=self._PORT, connect=False).server_info()
            else:
                self._CONN.server_info()
            return True
        except ServerSelectionTimeoutError as err:
            return False
