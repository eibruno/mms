from typing import Generator, Optional


from domain.enums.pair_enum import PairEnum, RangeEnum
from domain.interfaces.database import IDatabase
from domain.models.mms_model import MmsModel


class MmsRepository:
    def __init__(self, db: IDatabase):
        assert isinstance(db, IDatabase), "db should be an instance of db"
        self.__db = db

    def health_check(self):
        return self.__db.health_check()

    def save(self, mms: MmsModel):
        """
        Save a Mms to the database, using upsert option
        :param mms:
        :return: None
        """
        assert isinstance(mms, MmsModel)
        self.__db.connection["mb"]["mms"].update_one(mms.get_identifier(), {"$set": mms.dict()}, upsert=True)

    def get_count(self) -> int:
        """
        Get counter of items in database
        :return: int
        """
        return self.__db.connection["mb"]["mms"].find({}).count()

    def find_range(
            self,
            pair: PairEnum,
            from_time: int,
            to_time: int,
            limit: Optional[RangeEnum] = None,
            range_precision: bool = True,
    ) -> Generator[MmsModel, None, None]:
        """
        :param pair: pair of the query
        :param from_time: timestamp from the beginning of the range
        :param to_time: timestamp to the end of the range
        :param limit: limit of the response
        :param range_precision: take the from_time and to_time to consideration on the results
        :return: Generator[MmsModel, None, None]
        """

        assert isinstance(pair, PairEnum), "pair should be PairEnum"
        assert limit is None or isinstance(limit, RangeEnum), "range should be RangeEnum"
        assert isinstance(from_time, int), "from_time should be int"
        assert isinstance(to_time, int), "to_time should be int"
        assert isinstance(range_precision, bool), "range_precision should be int"

        query = {"pair": pair}

        if range_precision:
            query["timestamp"] = {"$lte": to_time, "$gte": from_time}
        else:
            query["timestamp"] = {"$lt": to_time, "$gt": from_time}

        cursor = self.__db.connection["mb"]["mms"].find(query)
        if limit:
            cursor.limit(limit)

        for item in cursor:
            yield MmsModel(**item)
