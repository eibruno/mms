import logging
from datetime import datetime
from json import JSONDecodeError
from typing import Generator
import socket

import requests
from circuitbreaker import circuit

from domain.exceptions.errors import RequestError
from domain.models.candle_model import CandleModel


class CandlesApi:
    def __init__(self) -> None:
        self._host = "mercadobitcoin.com.br"
        self._base_url = "http://mobile." + self._host + "/v4/{pair}/candle?from={from_time}&to={to_time}&precision=1d"
        # self._base_url = "https://bitcoin.free.beeceptor.com/v4/{pair}/candle?from={from_time}&to={to_time}&precision=1d"

    def health_check(self):
        try:
            socket.setdefaulttimeout(3)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self._host, 80))
        except OSError as error:
            return False
        else:
            s.close()
            return True

    @circuit
    def get_range(self, pair: str, from_time: datetime, to_time: datetime) -> Generator[CandleModel, None, None]:
        url = self._base_url.format(pair=pair, from_time=from_time, to_time=to_time)
        response = requests.request("GET", url)

        if response.status_code == 200:
            try:
                body = response.json()
                candles = body["candles"]
                for item in sorted(candles, key=lambda k: k['timestamp']):
                    yield CandleModel(**item)
                return
            except JSONDecodeError as err:
                logging.error(err)
        raise RequestError(
            f"Error while attempting to fetch candles api, error_code: {response.status_code} body: {response.text}"
        )
