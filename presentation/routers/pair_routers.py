from typing import List

from fastapi import APIRouter, Query
from fastapi.responses import JSONResponse

from domain.enums.pair_enum import PairEnum, RangeEnum
from domain.factories.database_factory import DatabaseFactory
from domain.models.pair_model import PairRequestModel, PairResponseModel
from domain.services.mms_service import MmsService
from infra.candles_api import CandlesApi

router = APIRouter(
    prefix=""
)

mms_service = MmsService(DatabaseFactory.get_implementation())
candle_api = CandlesApi()


@router.get("/hc")
def health_check():
    return mms_service.health_check() and candle_api.health_check()


@router.get("/{pair}/mms",
            responses={
                201: {"model": List[PairResponseModel], "description": "User Created Successfully"},
                400: {"model": List[PairResponseModel], "description": "Create User Failed"},
            })
def get_mms(pair: PairEnum, from_field: int = Query(default=None, alias="from"), to_field: int = Query(default=None, alias="to"), range_field: int = Query(default=None, alias="range")):
    try:
        request = PairRequestModel(pair=pair, from_time=from_field, to_time=to_field, range=range_field)
        response = []
        for item in mms_service.get_range(request.pair, request.from_time, request.to_time):
            mms = 0

            if request.range == RangeEnum.FIFTY:
                mms = item.mms_50
            if request.range == RangeEnum.TWENTY:
                mms = item.mms_20
            if request.range == RangeEnum.TWO_HUNDRED:
                mms = item.mms_200

            if mms == 0:
                continue

            response.append(PairResponseModel(
                timestamp=item.timestamp,
                mms=mms
            ))

        return response
    except Exception as exc:
        return JSONResponse(status_code=500, content={"message": str(exc)})
