import logging
from datetime import datetime

from dateutil.relativedelta import relativedelta

from domain.enums.pair_enum import PairEnum
from domain.interfaces.database import IDatabase
from domain.services.mms_service import MmsService
from infra.candles_api import CandlesApi


class IncrementWorker:
    def __init__(self, db: IDatabase):
        self.candles_api = CandlesApi()
        self.mms_service = MmsService(db)

    def load(self, force: bool = False):
        now_time = datetime.now()
        if self.mms_service.get_count() == 0 or force:
            logging.info("increment_worker: loading past 12 months")
            return self.start(now_time - relativedelta(month=12), now_time)

        logging.info("increment_worker: loading past 1 day")
        self.start(now_time - relativedelta(days=1), now_time)

    def start(self, from_time: datetime, to_time: datetime):
        for pair in PairEnum.cursor():
            logging.info(f"running on pair {pair} from {from_time} to {to_time}")
            candles = list(self.candles_api.get_range(pair, from_time, to_time))
            logging.info(f"saving {len(candles)} candles from candles_api")
            self.mms_service.save_mms(candles, pair)
