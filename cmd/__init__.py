import argparse
from datetime import datetime

from domain.factories.database_factory import DatabaseFactory
from presentation.workers.increment_worker import IncrementWorker


def parse_args() -> bool:
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--load", action="store_true", required=False,
                        help="load database from range or past 12 months if needed"
                        )
    parser.add_argument("-f", "--force", action="store_true", help="force load", required=False)
    parser.add_argument("--start", type=int, required=False)
    parser.add_argument("--end", type=int, required=False)
    args = parser.parse_args()

    if args.load:
        if args.start and not args.end or not args.start and args.end:
            raise ValueError("if you provide start you should provide end too. And vice-versa.")
        worker = IncrementWorker(DatabaseFactory.get_implementation())
        if args.start and args.end and not args.force:
            return worker.start(datetime.fromtimestamp(args.start), datetime.fromtimestamp(args.end))
        worker.load(args.force or False)
        return True
    return False
